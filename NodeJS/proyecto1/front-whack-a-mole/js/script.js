var score = 0;
var nivel = 1;
var intentos = 0;
var maxIntentos = 5;
var nickname = "";

var posMole = -1;
var timeShowMole = 2000;

var finJuego = false;

var myTimeout = null;

function getNickname(){
  nickname = location.search.split('nickname=')[1] ? location.search.split('myParam=')[1] : 'name';
}

function updateScore(score){
  document.getElementById("score").textContent = score;
}

function updateIntentos(v){
  document.getElementById("intentos").textContent = v;
}

function updateIntentosMax(v){
  document.getElementById("maxIntentos").textContent = v;
}

function updateMessage(m){
  document.getElementById("msggame").textContent = m;
}

function showHideEndBtns(){
  $("#endgame").toggle();
}

function showGame(){
  $(".loading").hide();
  $(".game").show();
}

function showMole(){
  posMole = getRandomArbitrary(0, 19);

  $( ".cuadro" ).each(function( index ) {
    if ( index ==  posMole) {
      $(this).addClass("castor");
    } else {
      $(this).removeClass("castor");
    }
  });

}

function getRandomArbitrary(min, max) {   
  return Math.round(Math.random() * (max - min) + min); 
}

function capturarClick(){
  $(".cuadro").click(function (){

    if (finJuego) {
      return;
    }

    var indexCuadro = $(this).attr("id");

    if (indexCuadro == posMole) {
      score++;
    } else {
      
      if (score > 0) {
        score--;
      }

      intentos++;
    }

    if (intentos >= maxIntentos) {
      finJuego = true;
      updateMessage("Fin del juego ...")

      showHideEndBtns();

      $.ajax({
        method:'POST',
        url:"http://localhost:3000/score",
        data: {
          'nickname' : nickname,
          'score': score,
          'nivel': nivel
        }
      }).then(function( data ) {
         console.log("se actualizo ...")
      });
      
    }

    updateScore(score);
    updateIntentos(intentos);
  });
}

function startGame() {
    updateScore(score);
    showGame();
    updateIntentosMax(maxIntentos);
    updateIntentos(intentos);

    getNickname();

    

    myTimeout = setInterval(showMole, timeShowMole);
    capturarClick();
}

function resetGame(){
  clearInterval(myTimeout);

  score = 0;
  intentos = 0;
  posMole = -1;
  finJuego = false;
  nivel = 1;
  startGame();
  showHideEndBtns();

}

window.onload = startGame;