import React from 'react';
import { BrowserRouter, Routes, Route } from 'react-router-dom';
import Search from './components/search';

const App = () => {
  return (
    <BrowserRouter>
      <div className="container-fluid">
        <Routes>
          <Route path="/" element={<Search />} />
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
