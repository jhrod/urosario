import React from 'react';

import Nav from 'react-bootstrap/Nav';

class NavGuestCustom extends React.Component{
    render (){
        return (
        <Nav>
            <Nav.Item>
              <Nav.Link href="/login">Login</Nav.Link>
            </Nav.Item>
            <Nav.Item>
              <Nav.Link href="/register">Registro</Nav.Link>
            </Nav.Item>
        </Nav>);
    }
}

export default NavGuestCustom;