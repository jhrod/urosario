import React from 'react';

import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';

import NavCustom from './nav.component'
import NavGuestCustom from './navguest.component'


import logo from './../../house.png';

import './menu.css'; 

class Menu extends React.Component{
    constructor(props){
        super(props);

        this.state = {};
    }


    render(){
        return(

    <Row>

        <Col sm={8}>  

            { (this.props.auth) ? <NavCustom/> :  <NavGuestCustom/> }

        </Col>

        <Col sm={4}>
            <img src={logo} class="logo" alt="Icono de la app"/>
        </Col>
    </Row>
            
        );
    }
}

export default Menu;

