import React from 'react';

import Form from 'react-bootstrap/Form';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';

import Button from 'react-bootstrap/Button';

import Menu from './../../components/menu/menu.component'

import axios from 'axios';

import {
    useNavigate,
    useParams,
  } from "react-router-dom";

function withParams(Component) {
    return props => <Component {...props} params={useParams()} />;
}

function withNavigation(Component) {
    return props => <Component {...props} navigate={useNavigate()} />;
}

class Products extends React.Component{

    constructor(props){
        super(props);

        this.state = {
            inmueble: null,
            cantidad: null,
            fecha: null,
            hora_inicio: null,
            hora_fin: null,
        }

    }

    loadProductData(){
        axios.get('http://localhost:5000/products/'+this.state.product_id).
            then( (response) => {

                if (!response.data) {
                    return 
                }

                let data = response.data
                
                let state = this.state

                state.name = data.product_name
                state.price = data.price
                this.setState(state)
                    
        })
    }

    onChangeInmueble(event){
        let state = this.state
        state.inmueble =  event.target.value
        this.setState(state)
    }

    onChangeCantidad(event){
        let state = this.state
        state.cantidad =  event.target.value
        this.setState(state)
    }

    onChangeFecha(event){
        let state = this.state
        state.fecha =  event.target.value
        this.setState(state)
    }

    onChangeStartHour(event){
        let state = this.state
        state.hora_inicio =  event.target.value
        this.setState(state)
    }

    onChangeFinishHour(event){
        let state = this.state
        state.hora_fin =  event.target.value
        this.setState(state)
    }
    
    handleForm(event){
        event.preventDefault();

        let data = {
            inmueble: this.state.inmueble,
            cantidad: parseInt(this.state.cantidad),
            fecha: this.state.fecha,
            hora_inicio: this.state.hora_inicio,
            hora_fin: this.state.hora_fin,
        }

        console.log(data)
            axios.post('http://localhost:5000/real_estate', data).
                then( function (){
                    alert("Property Add !!!")
            }).catch(function () {
                    alert("Error inesperado !!!")
            })
        
        
    }

    render () {
        return (
        <>
            <Menu auth={this.props.auth}/>

            <Row>

                <Col sm={2}></Col>
                <Col sm={8}>
                    <Form onSubmit={this.handleForm.bind(this)}>
                        
                        <Form.Group className="mb-3">
                            <Form.Label>Property</Form.Label>
                            <Form.Control
                                readonly={false} 
                                required
                                value= {this.state.inmueble}  
                                type="text" 
                                placeholder="Property Name: " 
                                onChange={this.onChangeInmueble.bind(this)}
                            />
                        </Form.Group>

                        <Form.Group className="mb-3" >
                            <Form.Label>Maximum Quantity of Visitors</Form.Label>
                            <Form.Control
                                required
                                type="number"
                                value= {this.state.cantidad} 
                                placeholder="Number of Visitors"
                                onChange={this.onChangeCantidad.bind(this)}
                            />
                        </Form.Group>

                        <Form.Group className="mb-3">
                            <Form.Label>Date of visite</Form.Label>
                            <Form.Control
                                required
                                type="date"
                                value= {this.state.price} 

                                placeholder="Date"
                                onChange={this.onChangeFecha.bind(this)}
                            />
                        </Form.Group>

                        <Form.Group className="mb-3">
                            <Form.Label>Start Hour</Form.Label>
                            <Form.Control
                                required
                                type="text"
                                value= {this.state.price} 

                                placeholder="Starting Hour of Visite"
                                onChange={this.onChangeStartHour.bind(this)}
                            />
                        </Form.Group>

                        <Form.Group className="mb-3">
                            <Form.Label>Finishing Hour</Form.Label>
                            <Form.Control
                                required
                                type="text"
                                value= {this.state.price} 

                                placeholder="Finishing Hour of Visite"
                                onChange={this.onChangeFinishHour.bind(this)}
                            />
                        </Form.Group>
                        <Button type="submit">
                             Publish
                        </Button>

                    </Form>
                </Col>
                <Col sm={2}></Col>
            </Row>
            
            


        </>);
    }
}


export default withParams(Products);