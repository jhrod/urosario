import React from 'react';

import Menu from './../../components/menu/menu.component'

import Table from 'react-bootstrap/Table';

import { FiXSquare } from 'react-icons/fi';
import { FiEdit2 } from 'react-icons/fi';

import { Link } from "react-router-dom";

import axios from 'axios';

class Home extends React.Component{

    constructor(props) {
        super(props);

        this.state = {
            list: []
        }

        this.consultar();
    }

    consultar(){
        fetch('http://localhost:5000/real_estate')
        .then(response => response.json())
        .then( (data) => {
            let state = this.state;
            state.list = data;
            this.setState(state);
        console.log(data);
        });
    }

    reservar(id, i){ 
        let confirmation = window.confirm('Do you want to reserve?') ? true : false
        let data = {
            _id: id,
            disponible: false,
        }
        console.log(data);
        var temp = this;
        if (confirmation) {
            axios.put('http://localhost:5000/real_estate/update', data).
                then( function (res){
                    console.log(res);
                    var list = temp.state.list;
                    list.splice(i,1);
                    temp.setState({list: list})
                    console.log(list);
                    alert("You reserve this property")
            }).catch(function (err) {
                console.log(err);
                alert("This property can not be reserve")
            })
        }
    }

    render () {
        return (
        <>
            <Menu auth={this.props.auth}/>

            <Table striped bordered hover>
            <thead>
                <tr>
                <th>Name of the Property</th>
                <th>Quantity of Visitors</th>
                <th>Date</th>
                <th>Starting Hour</th>
                <th>Finishing Hour</th>
                <th>  </th>
                </tr>
            </thead>
            <tbody>
                
                {
                    this.state.list.map((prodct,index) =>
                    <tr>
                        <td>{prodct.inmueble}</td>
                        <td>{prodct.cantidad}</td>
                        <td>{prodct.fecha}</td>
                        <td>{prodct.hora_inicio}</td>
                        <td>{prodct.hora_fin}</td>

                        <td>
                            
                        <button onClick={() => this.reservar(prodct._id, index)} >
                            Reserve
                        </button>

                        </td>
                    </tr>
                  )
                }
                
            </tbody>
            </Table>
        </>);
    }
}

export default Home;