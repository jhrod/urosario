
import React from "react";
import PropTypes from 'prop-types';
import './login.css';

import Form from 'react-bootstrap/Form';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Alert from 'react-bootstrap/Alert';

import Button from 'react-bootstrap/Button';

import Menu from './../../components/menu/menu.component'

import axios from 'axios';

import { config } from "../../config";

class Login extends React.Component {
    constructor(props){
        super(props);

        this.state = {
            errorLogin: false
        };
    }

    handleForm(event){
        event.preventDefault();

        let data = {
            email: this.state.userName,
            password: this.state.password,
        }
        console.log(config.host+ '/api/login')
        console.log(data)

        axios.post(config.host+ '/api/login', data).
                then((response) =>{
                
                if (response.status == 402){
                    this.setShowErrorLogin()
                } else {
                    this.props.setAuth(response);
                }
            }).catch((error) =>{
                console.log('respeusta ', error);
                if (error.response && error.response.status == 402){
                    this.setShowErrorLogin()
                } else {
                    alert('Error inesperado!')
                }
        })
    }

    onChangeUserName(event){
        let state = this.state
        state.userName =  event.target.value
        this.setState(state)
    }

    onChangePassword(event){
        let state = this.state
        state.password =  event.target.value
        this.setState(state)
    }

    setShowErrorLogin(){
        let state = this.state
        state.errorLogin = !state.errorLogin
        this.setState(state)

    }

    showErrorLogin(){
        return ( 
        
        <Alert variant="danger" onClose={() => this.setShowErrorLogin()} dismissible>
            <Alert.Heading>Usuario o contraseña incorrecto</Alert.Heading>
            <p>
            Por favor, intente de nuevo.
            </p>
        </Alert>
    );
    }


    render() {
      return (
        <>
        <Menu auth={this.props.auth}/>

        <Row>

            <Col sm={2}></Col>
            <Col sm={8}>
                <Form onSubmit={this.handleForm.bind(this)}>
                    
                    { this.state.errorLogin ? this.showErrorLogin() : null}

                    <Form.Group className="mb-3">
                        <Form.Label>Usuario</Form.Label>
                        <Form.Control
                            readonly={false} 
                            required
                            value= {this.state.inmueble}  
                            type="text" 
                            placeholder="Username - email" 
                            onChange={this.onChangeUserName.bind(this)}
                        />
                    </Form.Group>

                    <Form.Group className="mb-3" >
                        <Form.Label>Contraseña</Form.Label>
                        <Form.Control
                            required
                            type="password"
                            value= {this.state.cantidad} 
                            placeholder="Contraseña"
                            onChange={this.onChangePassword.bind(this)}
                        />
                    </Form.Group>

                    
                    <Button type="submit">
                         Login
                    </Button>

                </Form>
            </Col>
            <Col sm={2}></Col>
        </Row>
    </>
      );
    }
}

Login.propTypes = {
    setAuth: PropTypes.func.isRequired
};

export default Login;