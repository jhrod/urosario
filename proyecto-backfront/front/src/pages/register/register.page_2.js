import React from "react";
import axios from "axios";

import Form from "react-bootstrap/esm/Form";
import Row from "react-bootstrap/esm/Row";
import Col from "react-bootstrap/esm/Col";
import Button from "react-bootstrap/esm/Button";

import Menu from "../../components/menu/menu.component";

import './register.css';

class Register extends React.Component {
    constructor (props) {
        super(props);
        this.state = {
            username: "default username",
            email: null,
            password: null,
        };
    }

    handleForm(event){
        event.preventDefault();
        let data = {
            username: this.state.username,
            email: this.state.email,
            password: this.state.password,
        };
        axios.post('http://localhost:5000/api/signup', data).then(function(response) {
            this.props.setAuth(response);
        }).catch(function(err) {
            alert("Error: "+ err);
        });
    }

    onChangeUserName(event){
        let state = this.state;
        state.username =  event.target.value;
        this.setState(state);
    }

    onChangeEmail(event){
        let state = this.state;
        state.email =  event.target.value;
        this.setState(state);
    }

    onChangePassword(event){
        let state = this.state;
        state.password =  event.target.value;
        this.setState(state);
    }

    render() {
        return (
            <>
                <Menu auth={this.props.auth}/>
                <h2>Registro</h2>
                <Row>
                    <Col sm={2}></Col>
                    <Col sm={8}>
                        <Form onSubmit={this.handleForm.bind(this)}>
                            <Form.Group className="mb-3">
                                <Form.Label>Nombre de usuario</Form.Label>
                                <Form.Control
                                    readonly={false} 
                                    required
                                    type="text" 
                                    placeholder="nombre de usuario"
                                    onChange={this.onChangeUserName.bind(this)} 
                                />
                            </Form.Group>
                            <Form.Group className="mb-3">
                                <Form.Label>Correo electrónico</Form.Label>
                                <Form.Control
                                    readonly={false} 
                                    required
                                    type="email" 
                                    placeholder="email" 
                                    onChange={this.onChangeEmail.bind(this)}
                                />
                            </Form.Group>

                            <Form.Group className="mb-3" >
                                <Form.Label>Contraseña</Form.Label>
                                <Form.Control
                                    required
                                    type="password"
                                    placeholder="contraseña"
                                />
                            </Form.Group>

                            <Form.Group className="mb-3" >
                                <Form.Label>Confirmar contraseña</Form.Label>
                                <Form.Control
                                    required
                                    type="password"
                                    placeholder="Contraseña"
                                />
                            </Form.Group>

                            <Button type="submit">
                                Registrarse
                            </Button>
                        </Form>
                    </Col>
                    <Col sm={2}></Col>
                </Row>
            </>
        );
    }
}

export default Register;