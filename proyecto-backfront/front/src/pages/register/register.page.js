import React, { Component } from 'react';

import Menu from '../../components/menu/menu.component';

import './register.css';

export default class Register extends Component {
  render () {
    return (
        <>
            <Menu auth={this.props.auth}/>
            <div className="Registro">
                <h2> Registro </h2>
                <form className="datos_inmueble" onSubmit= {this.onSubmit}>
                    <label> Correo </label>
                    <br/>
                    <input type = "text" className="espacios" />
                    <br/>
                    <label> Nombre </label>
                    <br/>
                    <input type = "text" className="espacios" />
                    <br/>
                    <label> Apellido </label>
                    <br/>
                    <input type = "text" className="espacios" />
                    <br/>
                    <label> Documento de identidad </label>
                    <br/>
                    <input type = "text" className="espacios" />
                    <br/>
                    <label> Tipo de documento </label>
                    <br/>
                    <input type = "text" className="espacios" />
                    <br/>
                    <label> Telefono </label>
                    <br/>
                    <input type = "text" className="espacios" />
                    <br/>
                    <label> Contrasena </label>
                    <br/>
                    <input type = "text" className="espacios" />
                    <br/>
                    <input type = "submit" value="Guardar" id="enviar"/>
                </form>
            </div>
        </>
    );
  }
}