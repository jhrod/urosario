import './App.css';

import Container from 'react-bootstrap/Container';

import Home from './pages/home/home.page'
import Products from './pages/products/products.page'
import Login from './pages/login/login.page'
import Register from './pages/register/register.page'

import {
  BrowserRouter,
  Routes,
  Route,
} from "react-router-dom";

import useAuth from './auth';
import { ProtectedRoute } from './components/protect-route';

function App() {
  const { auth, setAuth } = useAuth();
  
  console.log(auth);

  return (
    <div className="App">
      <Container fluid>
        <BrowserRouter>
            <Routes>
              
              <Route path="/login" element={<Login auth={auth} setAuth={setAuth} />} />
              <Route path="/register" element={<Register />} />

              <Route path="/" element={ <ProtectedRoute auth={auth}>
                  <Home auth={auth}/>
              </ProtectedRoute>} />
              
              <Route path="/reserva/create" element={            
                <ProtectedRoute auth={auth}>
                  <Products  auth={auth}/>
                </ProtectedRoute>} 
              />

              <Route path="/reserva/:id/update"  element={
                <ProtectedRoute auth={auth}>
                <Products />
              </ProtectedRoute>} 
              />

            </Routes>
        </BrowserRouter>

      </Container>
    </div>
  );
}

export default App;
