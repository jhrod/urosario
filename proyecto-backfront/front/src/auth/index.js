import { useState } from 'react';

var keyAuth = "apptoken";

export default function useAuth() {
  const getAuth = () => {
    const authString = localStorage.getItem(keyAuth);
    const userAuth = JSON.parse(authString);
    return userAuth ? userAuth : null
  };

  const [auth, setAuth] = useState(getAuth());

  const saveAuth = item => {
    localStorage.setItem(keyAuth, JSON.stringify(item));
    setAuth(item);
  };

  return {
    setAuth: saveAuth,
    auth,
  }
}
