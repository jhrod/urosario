const mongoose = require('mongoose')
const connectionString = process.env.DB_CONNECTION;


const config = {useNewUrlParser: true};

let connection;

module.exports = {
  connectToServer: function (callback) {
    mongoose.connect(connectionString, config, function (err, dbObj) {
      if (err || !dbObj) {
        return callback(err);
      }

      console.log('Successfully connected to MongoDB.');

      return callback();
    });
  }
};