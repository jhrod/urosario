'use strict'
const express = require('express')

const api = express.Router()

const isAuth = require('../middlewares/auth')

//controllers
const UserController = require('../controllers/userController')

// user
api.post('/login', UserController.signIn)
api.post('/signup', UserController.signUp)
api.get('/users', isAuth, UserController.getUsers)

module.exports = api