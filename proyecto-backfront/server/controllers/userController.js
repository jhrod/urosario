'use strict'

const User = require('../models/user')
const jwt = require('../middlewares/jwt')


function signUp( req, res ){
    const user = new User({
        email: req.body.email,
        name: req.body.name,
        last_name: req.body.last_name,
        document: req.body.document,
        type_document: req.body.type_document,
        phone: req.body.phone,
        password: req.body.password
    })

    User.findOne({email: req.body.email}, (err, userRegistered) => {
        if(err){
            return res.status(500).send({message:`Error ${err}`})
        }

        if (userRegistered) {
            return res.status(409).send({message: `${req.body.email}, Ya hay un usuario con ese correo`})
        }

        const user = new User({
            email: req.body.email,
            name: req.body.name,
            last_name: req.body.last_name,
            role: req.body.role,
            group: req.body.group,
            dealer: req.body.dealer,
            password: req.body.password
        })

        user.save((err) => {
            if (err) {
                return res.status(500).send({message:`Error al crear el usuario: ${err}`})
            }

            return res.status(200).send({token: jwt.createToken(user), user: user})
        })

    })
}

function signIn( req, res ){
    let userEmail = req.body.email
    let userPassword = req.body.password

    User.findOne({ email: userEmail}, (err, user) => {
        if(err) {
            return res.status(500).send({message:`No se pudo hacer conexión con el servidor, Por favor confirme al acceso a la siguiente url: https://app.kia.com.co:3001/api/private`, err:err})
        }

        if(!user) {
            return res.status(402).send({message:'Usuario o contraseña incorrecto'})
        }

        req.user = user

        req.user.comparePass(userPassword, (err, isMatch )=> {

        if(isMatch) {
            return res.status(200).send({
                message: 'Login ok',
                user: user,
                token: jwt.createToken(user)
            })
        }

        res.status(402).send({message: `Usuario o contraseña incorrecto`})
        })
    })
}

function getUsers(req, res){
    User.aggregate([
        { $sort:{ last_name:1 } }
    ], (err, users) =>{
        if(err) return res.status(500).send({message:`Error al realizar la petición ${err}`})
        if(!users) return res.status(404).send({message:`No existen Usuarios`})
        res.status(200).send(users)
    })
}

module.exports = {
    signUp,
    signIn,
    getUsers
}