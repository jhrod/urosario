const express = require('express');
const { ObjectId } = require('mongodb');

const estateRoutes = express.Router();
const db = require('../db/connection');

var ObjectID = require('mongodb').ObjectID

var collectionName = "estate";

estateRoutes.route('/real_estate').get(async function (_req, res) {
  const dbConnect = db.getDb();

  dbConnect
    .collection(collectionName)
    .find({disponible:true})
    .toArray(function (err, result) {
      if (err) {
        res.status(400).send('Error finding the items!');
      } else {
        res.json(result);
      }
    });
});


estateRoutes.route('/real_estate').post(function (req, res) {
  const dbConnect = db.getDb();
  const data = {
    inmueble: req.body.inmueble,
    cantidad: req.body.cantidad,
    fecha : req.body.fecha,
    hora_inicio: req.body.hora_inicio,
    hora_fin: req.body.hora_fin,
    disponible: true,
  };

  dbConnect
    .collection(collectionName)
    .insertOne(data, function (err, result) {
      if (err) {
        res.status(400).send('Error inserting!');
      } else {
        let m = `The estate was added`
        res.status(200).send({message: m});
      }
    });
});

estateRoutes.route('/real_estate/update').put(function (req, res) {
  const dbConnect = db.getDb();
  const query = { _id: new ObjectId(req.body._id) };
  console.log(req.body);
  if (req.body.disponible){
    res.status(404).send({message: "The estate is not available"});
  }
  else{
    const updates = { $set: {
      disponible: false,
     }
   };

   dbConnect
     .collection(collectionName)
     .updateOne(query, updates, function (err, _result) {
       if (err) {
         res
           .status(400)
           .send({ message: `Error updating item with id ${query.id}!`});
       } else {
         let m = `Document updated`
         res.status(200).send({message: m});
       }
     });
  }

});

module.exports = estateRoutes;
