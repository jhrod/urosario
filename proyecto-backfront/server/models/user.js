'use strict'

const mongoose = require('mongoose')
const Schema = mongoose.Schema
const bcrypt = require('bcrypt-nodejs')
const crypto = require('crypto')

const UserSchema = new Schema({
    email: { type:String, unique: true, lowercase: true},
    name: {type:String, lowercase: true},
    last_name: {type:String, lowercase: true},
    active: {type:Boolean, default:true},
    role: String,
    type_doc: { type: String},
    document: {type: String},
    phone: {type: String},
    avatar: String,
    password: {type:String, select: true},
    singUpDate: {type: Date, default: Date.now() },
    lastLogin: Date
})

UserSchema.pre('save', function (next){
    let user = this

    if(!user.isModified('password')) return next()

    bcrypt.genSalt(10, (err, salt)=>{
        if(err) return next(err)

        bcrypt.hash(user.password, salt, null, (err, hash)=>{
            if(err) return next(err)
            user.password = hash
            next()
        })
    })
})


UserSchema.methods.comparePass = function(password, cb){
    bcrypt.compare(password, this.password, (err, isMatch)=>{
        if(err) {return cb(err);}
        return cb(null, isMatch)
    })
}

module.exports = mongoose.model('User', UserSchema)

