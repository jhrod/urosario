import './App.css';

import Container from 'react-bootstrap/Container';

import Home from './pages/home/home.page'
import Products from './pages/products/products.page'

import {
  BrowserRouter,
  Routes,
  Route,
} from "react-router-dom";

function App() {
  return (
    <div className="App">
      <Container fluid>
        <BrowserRouter>
            <Routes>
              <Route path="/" element={<Home />} />
              <Route path="/products/create" element={<Products />} />
              <Route path="/products/:id/update"  element={<Products />} />
            </Routes>
        </BrowserRouter>

      </Container>
    </div>
  );
}

export default App;
