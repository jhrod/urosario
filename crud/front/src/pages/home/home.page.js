import React from 'react';

import Menu from './../../components/menu/menu.component'

import Table from 'react-bootstrap/Table';

import { FiXSquare } from 'react-icons/fi';
import { FiEdit2 } from 'react-icons/fi';

import { Link } from "react-router-dom";

import axios from 'axios';

class Home extends React.Component{

    constructor(props) {
        super(props);

        this.state = {
            list: []
        }

        this.consultar();
    }

    consultar(){
        fetch('http://localhost:5000/products')
        .then(response => response.json())
        .then( (data) => {
            let state = this.state;
            state.list = data;
            this.setState(state)
        });
    }

    deleteProduct(id){ 
        let confirmation = window.confirm('Esta seguro de eliminar este item?') ? true : false

        if (confirmation) {
            axios.delete('http://localhost:5000/products/delete/'+id).
                then( () => {
                    alert("Producto eliminado!!")
                    window.location.reload()
            }).catch(function () {
                alert("Error inesperado !!!")
            })
        }
    }

    render () {
        return (
        <>
            <Menu/>

            <Table striped bordered hover>
            <thead>
                <tr>
                <th>#</th>
                <th>Nombre producto</th>
                <th>Precio</th>
                <th>Fecha Act</th>

                <th>  </th>
                </tr>
            </thead>
            <tbody>
                
                {
                    this.state.list.map((prodct) =>
                    <tr>
                        <td>{prodct.product_id}</td>
                        <td>{prodct.product_name}</td>
                        <td>{prodct.price}</td>
                        <td>{prodct.last_modified ? prodct.last_modified : "no mod." }</td>

                        <td>
                            
                        <button onClick={() => this.deleteProduct(prodct.product_id)} >
                            <FiXSquare/>
                        </button>

                        <Link to={"/products/" + prodct.product_id + "/update"}>
                            <FiEdit2/>
                        </Link>

                        </td>
                    </tr>
                  )
                }
                
            </tbody>
            </Table>
        </>);
    }
}

export default Home;