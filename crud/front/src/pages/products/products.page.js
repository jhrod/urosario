import React from 'react';

import Form from 'react-bootstrap/Form';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';

import Button from 'react-bootstrap/Button';

import Menu from './../../components/menu/menu.component'

import axios from 'axios';

import {
    useNavigate,
    useParams,
  } from "react-router-dom";

function withParams(Component) {
    return props => <Component {...props} params={useParams()} />;
}

function withNavigation(Component) {
    return props => <Component {...props} navigate={useNavigate()} />;
}

class Products extends React.Component{

    constructor(props){
        super(props);

        this.state = {
            name: null,
            price: null,
            product_id: (this.props.params.id) ? this.props.params.id : null,
            isNew: (this.props.params.id) ? false : true
        }

        if (!this.state.isNew){
            this.loadProductData()
        }
    }

    loadProductData(){
        axios.get('http://localhost:5000/products/'+this.state.product_id).
            then( (response) => {

                if (!response.data) {
                    return 
                }

                let data = response.data
                
                let state = this.state

                state.name = data.product_name
                state.price = data.price
                this.setState(state)
                    
        })
    }

    onChangeProductId(event){
        let state = this.state
        state.product_id =  event.target.value
        this.setState(state)
    }

    onChangeProductName(event){
        let state = this.state
        state.name =  event.target.value
        this.setState(state)
    }

    onChangeProductPrice(event){
        let state = this.state
        state.price =  event.target.value
        this.setState(state)
    }

    handleForm(event){
        event.preventDefault();

        let data = {
            product_id: parseInt(this.state.product_id),
            product_name: this.state.name,
            price: parseInt(this.state.price)
        }

        console.log(data)

        if (this.state.isNew) {
            axios.post('http://localhost:5000/products/new', data).
                then( function (){
                    alert("Datos guardados !!!")
            }).catch(function () {
                    alert("Error inesperado !!!")
            })
        } else {
            axios.put('http://localhost:5000/products/update', data).
                then( function (){
                    alert("Datos actualizados !!!")
            }).catch(function () {
                alert("Error inesperado !!!")
            })
        }
        
    }

    render () {
        return (
        <>
            <Menu/>

            <Row>

                <Col sm={2}></Col>
                <Col sm={8}>
                    <Form onSubmit={this.handleForm.bind(this)}>
                        
                        <Form.Group className="mb-3">
                            <Form.Label>Producto ID</Form.Label>
                            <Form.Control
                                readonly={true} 
                                required
                                value= {this.state.product_id}  
                                type="number" 
                                placeholder="Ejemplo: 1" 
                                onChange={this.onChangeProductId.bind(this)}
                            />
                        </Form.Group>

                        <Form.Group className="mb-3" >
                            <Form.Label>Nombre</Form.Label>
                            <Form.Control
                                required
                                type="text"
                                value= {this.state.name} 
                                placeholder="Nombre del producto"
                                onChange={this.onChangeProductName.bind(this)}
                            />
                        </Form.Group>

                        <Form.Group className="mb-3">
                            <Form.Label>Precio</Form.Label>
                            <Form.Control
                                required
                                type="number"
                                value= {this.state.price} 

                                placeholder="Precio del producto"
                                onChange={this.onChangeProductPrice.bind(this)}
                            />
                        </Form.Group>


                        <Button type="submit">
                            {this.state.isNew ? "Guardar" : "Actualizar"}
                        </Button>

                    </Form>
                </Col>
                <Col sm={2}></Col>
            </Row>
            
            


        </>);
    }
}


export default withParams(Products);