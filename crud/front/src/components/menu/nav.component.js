import React from 'react';

import Nav from 'react-bootstrap/Nav';

class NavCustom extends React.Component{
    render (){
        return (
        <Nav>
            <Nav.Item>
              <Nav.Link href="/">Listado</Nav.Link>
            </Nav.Item>
            <Nav.Item>
              <Nav.Link href="/products/create">Crear</Nav.Link>
            </Nav.Item>
        </Nav>);
    }
}

export default NavCustom;